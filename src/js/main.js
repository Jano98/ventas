import Vue from 'vue';
import App from './App.vue';

const app = new Vue({
    el: '#container',
    render: h => h(App)
})